module Graph where

import qualified Data.List as L
import qualified Data.Map.Strict as M

type NodeId = Int
data ConnectionType = Flowing | Separated | Open deriving (Show, Eq, Ord)
data Connection = Connection NodeId ConnectionType deriving (Show, Eq, Ord)
data Edge = End | Connected Connection deriving (Show, Eq, Ord)
data Node = Node NodeId [Edge] deriving (Show, Eq, Ord)
type Graph = M.Map Int Node

updatedGraph :: Graph -> NodeId -> Connection -> Maybe Graph
updatedGraph xs n c @ (Connection n' t) = do
  leftGraph <- updatedNode xs n c
  updatedNode leftGraph n' (Connection n t)

updatedNode :: Graph -> NodeId -> Connection -> Maybe Graph
updatedNode xs n c = do
  node @ (Node _ edges) <- M.lookup n xs
  updatedEdges <- updateEdges c edges
  return $ M.insert n (Node n updatedEdges) xs

updateEdges :: Connection -> [Edge] -> Maybe [Edge]
updateEdges c xs =
  let
    f :: Connection -> Edge -> ([Edge], Bool) -> ([Edge], Bool)
    f _ End (edges, updated) = (End : edges, updated)
    f _ e (edges, True) = (e : edges, True)
    f c @ (Connection i t) e @ (Connected (Connection i' _)) (edges, _)
      | i == i' = (Connected c : edges, True)
      | otherwise = (e : edges, False)
    (edges, updated) = L.foldr (f c) ([], False) xs
  in if updated then Just edges else Nothing
