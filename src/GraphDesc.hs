module GraphDesc where

import qualified Data.List as L
import qualified Data.Map.Strict as M

import Graph (
  Graph,
  Node(Node),
  Connection(Connection),
  Edge(End, Connected),
  ConnectionType(Open))

type NodeDesc = (Int, [Int])
type GraphDesc = [NodeDesc]

validateEdgeCount :: Int -> GraphDesc -> Bool
validateEdgeCount _ [] = True
validateEdgeCount c ((_, cs) : xs) = length cs == c && validateEdgeCount c xs

removeEdges :: GraphDesc -> GraphDesc
removeEdges [] = []
removeEdges ((n, cs) : xs) = (n, filter (/= 0) cs) : removeEdges xs

validGraph :: GraphDesc -> Bool
validGraph [] = True
validGraph ((_, []) : xs) = validGraph xs
validGraph ((n, c : cs) : xs) =
  case updateGraphDesc n c xs of
    Just xs' -> validGraph ((n, cs) : xs')
    Nothing -> False

updateGraphDesc :: Int -> Int -> GraphDesc -> Maybe GraphDesc
updateGraphDesc n c xs = do
  connectedN <- connectedNode c xs
  updatedN <- updatedNodeDesc n connectedN
  return $ replaceNodeDesc updatedN xs

connectedNode :: Int -> GraphDesc -> Maybe NodeDesc
connectedNode c = L.find (\(x, _) -> x == c)

updatedNodeDesc :: Int -> NodeDesc -> Maybe NodeDesc
updatedNodeDesc n (n', cs) =
  if length cs /= length cs'
    then Just (n', cs')
    else Nothing
  where
    cs' = L.delete n cs

replaceNodeDesc :: NodeDesc -> GraphDesc -> GraphDesc
replaceNodeDesc _ [] = []
replaceNodeDesc newN @ (n', cs') (oldN @ (n, cs) : xs)
  | n' == n = newN : replaceNodeDesc newN xs
  | otherwise = oldN : replaceNodeDesc newN xs

graphFromDesc :: GraphDesc -> Graph
graphFromDesc xs = L.foldl' f M.empty ns
  where
    f :: Graph -> Node -> Graph
    f acc n @ (Node i  _) = M.insert i n acc
    ns = map nodeFromDesc xs

nodeFromDesc :: NodeDesc -> Node
nodeFromDesc (n, cs) = Node n $ map f cs
  where
    f :: Int -> Edge
    f 0 = End
    f c = Connected $ Connection c Open
