module Generator where

type MandatoryEdgeCount = Int
data EdgeGroup = EdgeGroup MandatoryEdgeCount [NodeId]

pickConnections :: [EdgeGroup] -> [NodeId]
 {-
type
Edges [NodeId] -- has to take at least one
OptionalEdges [NodeId]  -- can take any
range: (number of mandatory edges) -> (elements) - 1
1. pick one from EdgeGroup -> remove from group until minimum goes to 0
2. decide how many more to pick, at most (sum max - sum min - 1)
3. create pool of all remaining EdgeGroups
  - pick one, decrease maximum of that EdgeGroup
  - if the maximum of an EdgeGroup reaches zero remove all elements from the pool
-}
