module GraphSpec where

import Test.Hspec

import Graph
import GraphData

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "updatedNode" $
    it "should update the connection" $
      updatedNode smallGraph 2 (Connection 4 Flowing) `shouldBe` Just halfUpdatedSmallGraph
  describe "updatedGraph" $
    it "should update both ends of the connection" $
      updatedGraph smallGraph 2 (Connection 4 Flowing) `shouldBe` Just updatedSmallGraph
