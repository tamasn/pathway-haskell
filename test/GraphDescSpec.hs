module GraphDescSpec where

import Test.Hspec

import GraphDesc
import GraphData

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "connectedNode" $ do
    it "finds the connected node" $
      connectedNode 2 graphWOEnds `shouldBe` Just (2, [3, 5, 1])
    it "return nothing for invalid connection" $
      connectedNode 10 graphWOEnds `shouldBe` Nothing
  describe "updatedNodeDesc" $ do
    it "correctly removes the matching element" $
      updatedNodeDesc 1 (2,[3,5,1]) `shouldBe` Just (2, [3,5])
    it "doesn't return anything if there's no match" $
      updatedNodeDesc 4 (2,[3,5,1]) `shouldBe` Nothing
  describe "replaceNodeDesc" $
    it "correctly replaces the node with the new one" $
      replaceNodeDesc (4, [3]) [(3, [4]), (4, [2, 3])] `shouldBe` [(3, [4]), (4, [3])]
  describe "updateGraphDesc" $
    it "should correctly find and replace an element" $
      updateGraphDesc 2 4 [(3, [4]), (4, [2, 3])] `shouldBe` Just [(3, [4]), (4, [3])]
  describe "validGraph" $ do
    it "should recognize a valid graph" $
      validGraph smallGraphWOEnds `shouldBe` True
    it "should recognize a bigger valid graph" $
      validGraph graphWOEnds `shouldBe` True
  describe "nodeFromDesc" $
    it "should properly convert to Node" $
      nodeFromDesc nodeDesc `shouldBe` node
  describe "graphFromDesc" $
    it "should properly convert to Graph" $
      graphFromDesc tinyGraphDesc `shouldBe` tinyGraph
