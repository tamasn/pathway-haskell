module GraphData where

import GraphDesc (GraphDesc, graphFromDesc, removeEdges, NodeDesc)
import Graph (
  Graph,
  Node (Node),
  Edge(End, Connected),
  Connection(Connection),
  ConnectionType(Open, Flowing))
import qualified Data.Map.Strict as M

graphDescription :: GraphDesc
graphDescription = [
  (1, [0, 2, 4, 0]),
  (2, [0, 3, 5, 1]),
  (3, [0, 0, 6, 2]),
  (4, [1, 5, 7, 0]),
  (5, [2, 6, 8, 4]),
  (6, [3, 0, 9, 5]),
  (7, [4, 8, 0, 0]),
  (8, [5, 9, 0, 7]),
  (9, [6, 0, 0, 8])]

smallGraphWOEnds :: GraphDesc
smallGraphWOEnds = [
  (1, [2,3]),
  (2, [1,4]),
  (3, [1,4]),
  (4, [2,3])]

smallGraphDesc :: GraphDesc
smallGraphDesc = [
  (1, [0,2,3,0]),
  (2, [0,0,4,1]),
  (3, [1,4,0,0]),
  (4, [2,0,0,3])]

nodeDesc :: NodeDesc
nodeDesc = (1, [0,2,3,0])

node :: Node
node = Node 1 [
  End,
  Connected (Connection 2 Open),
  Connected (Connection 3 Open),
  End]

tinyGraphDesc :: GraphDesc
tinyGraphDesc = [
  (1, [0,2]),
  (2, [1, 0])]

tinyGraph :: Graph
tinyGraph =
  M.insert 1 (Node 1 [End, Connected (Connection 2 Open)]) $
  M.insert 2 (Node 2 [Connected (Connection 1 Open), End]) M.empty

smallGraph = graphFromDesc smallGraphDesc

halfUpdatedSmallGraph =
  M.insert 2 (Node 2 [End, End, Connected (Connection 4 Flowing), Connected (Connection 1 Open)]) smallGraph

updatedSmallGraph =
  M.insert 2 (Node 2 [End, End, Connected (Connection 4 Flowing), Connected (Connection 1 Open)]) $
  M.insert 4 (Node 4 [Connected (Connection 2 Flowing), End, End, Connected (Connection 3 Open)]) smallGraph

graphWOEnds = removeEdges graphDescription
